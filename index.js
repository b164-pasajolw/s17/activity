

console.log('Activity 17');

let addStudent = []

function student(name) {
	addStudent.push(name)
	console.log(name + " was added to the students' list")
}


function countStudent() {
		console.log(`There are a  total of ${addStudent.length} students enrolled.`)
	}


function printStudents() {
	addStudent.sort();
	addStudent.forEach(function(student) {
		console.log(student)
	})

}

function findStudent(search) {

	let match = addStudent.filter(function(student) {
		return student.toLowerCase().includes(search.toLowerCase())
	})

	//conditional statement
	if (match.length == 1){
		console.log( `${match} is an Enrollee`);
	} else if(match.length > 1){
		console.log(`${match} are Enrollees`);
	} else {
		console.log(`No student found with the name ${search}`);
	}
}

//Stretch Goals:
//1. Add a section to the students
//2. Remove the name of the student

function addSection(section) {
	let sectionedStudents = students.map(function(student) {
		//returns the name of the student plust the added section

		return student + ' - section' + section;
	})

	//Prints the array of sectionedStudents for verification
	console.log(sectionedStudents);
}

function removeStudent(name) {
	//converts the first letter of the name to uppercase
	let firstLetter = name.slice(0, 1).toUpperCase();
	//Retrieves all other letters of the name
	let remainingLetters = name.slice(1, name.length);

	let studentName = firstLetter + remainingLetters;

	//retrieve the index of the name provided
	let studentIndex = students.indexOf(studentName);

	//if a match is found, the index number would be 0 or greater than
	//if the name is not found the result is -1
	if(studentIndex >= 0) {
		//to remove the index of the name provided
		students.splice(studentIndex, 1);
		console.log(name + " was removed from the student's list.")
	} else{
		console.log("no student found")
	}

}
